//
// Created by avk on 22.03.18.
//

#ifndef STUDENTTASKS2017_MAZEWALKERCAMERA_H
#define STUDENTTASKS2017_MAZEWALKERCAMERA_H


#include <glm/glm.hpp>
#include <Camera.hpp>
#include <memory>
#include "../src/Maze.h"

class MazeWalkerCamera : public FreeCameraMover {
public:
    explicit MazeWalkerCamera(MazePtr maze);
//    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
//
//    virtual void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) = 0;
//    virtual void handleScroll(GLFWwindow* window, double xoffset, double yoffset) {}
    void update(GLFWwindow* window, double dt) override;

private:
    struct DontMoveAxis {
        bool X = false;
        bool Y = false;
        bool Z = false;

        void updatePos(glm::vec3& old, glm::vec3 newPos) const {
            newPos.x = (X ? old.x : newPos.x);
            newPos.y = (Y ? old.y : newPos.y);
            newPos.z = (Z ? old.z : newPos.z);

            old = newPos;
        }

        DontMoveAxis& operator||(const DontMoveAxis& other) {
            return _or_(other);
        }

        DontMoveAxis& operator|=(const DontMoveAxis& other) {
            return (*this = _or_(other));
        }
    private:
        DontMoveAxis& _or_(DontMoveAxis other) {
            X |= other.X;
            Y |= other.Y;
            Z |= other.Z;
            return *this;
        }
    };

    DontMoveAxis _checkCollision_(const glm::vec3& newCameraPosition) const;

    DontMoveAxis _checkWallsCollision_(const glm::vec3& cameraPos) const;
    DontMoveAxis _checkFloorCollision_(const glm::vec3& cameraPos) const;
    DontMoveAxis _checkCeilCollision_(const glm::vec3& cameraPos) const;

    DontMoveAxis _checkOneWallCollision(const glm::vec3& cameraPos, const Wall& wall) const;
private:
    MazePtr _maze;
    const float COLLISION_DIST = 0.13f;
};

using MazeWalkerCameraPtr = std::shared_ptr<MazeWalkerCamera>;

#endif //STUDENTTASKS2017_MAZEWALKERCAMERA_H
