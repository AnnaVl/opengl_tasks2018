#pragma once

#include <string>
#include <stack>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

struct LSystemLine {
    int level;

    glm::vec3 a;
    glm::vec3 b;
};

class LSystem {

    struct State {
        glm::vec3 pos;
        glm::vec3 dir;
        glm::vec3 angles;

        float angle;
        float invert;
    };

    std::unordered_map<char, std::vector<std::string>>     rules;
    std::string         initialString;
    float               angle;
    float               distScale;
    float               angleScale;
    std::string         currentString;
    std::vector<glm::vec3> points;
    std::vector<LSystemLine> lines;

public:

    LSystem();

    void setInitialString(std::string str) {
        initialString = std::move(str);
    }

    void addRule(char symbol, std::string rule) {
        rules[symbol].emplace_back(std::move(rule));
    }

    void setAngle(float newAngle) {
        angle = newAngle;
    }

    std::string getCurrentString() const {
        return currentString;
    }

    void setDistScale(float newScale) {
        distScale = newScale;
    }

    void setAngleScale(float scale) {
        angleScale = scale;
    }

    void interpretString(std::string str);

    void buildSystem(int numIterations);

    std::vector<LSystemLine> draw() {
        lines.clear();
        interpretString(currentString);
        return std::move(lines);
    }

protected:
    std::string oneStep(std::string in) const;

    glm::vec3 step(const State& state) const;

    virtual void updateState(State& state, const glm::vec3& dir, int level) const;
};
