#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "lsys.hpp"
#include "tree.hpp"

namespace {
float frand(float a, float b) {
    static std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(a, b);
    return distribution(generator);
}
}


class SampleApplication : public Application
{
public:
    const int NUM_TREES = 50;

    std::pair<MeshPtr, MeshPtr> _tree_and_leaves;
    MeshPtr _sphere;
    MeshPtr _ground;

    ShaderProgramPtr _shader;

    float _phi = glm::pi<float>() * 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    TexturePtr _barkTexture;
    TexturePtr _grassTexture;
    TexturePtr _leafTexture;

    GLuint _sampler;
    GLuint _grassSampler;
    GLuint _leafSampler;

    std::vector<glm::vec3> _positionsVec3;

    void makeScene() override {
        Application::makeScene();

        LSystem lsystem;
        lsystem.setInitialString ("F");
        lsystem.addRule('F', "F[-F]&[F]^^[F]&[+F][F]");
        lsystem.buildSystem(3);
        _tree_and_leaves = makeTreeAndLeaves(lsystem.draw());
        _tree_and_leaves.first->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, -2.0f, 0.0f)));
        _tree_and_leaves.second->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, -2.0f, 0.0f)));

        ////////////////////////

        const float size = 20.0f;
        for (unsigned int i = 0; i < NUM_TREES; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand(-size, size), 0, frand(-size, size)));
            std::cerr << _positionsVec3.back().x << " " << _positionsVec3.back().z << std::endl;
        }

        _ground = makeGroundPlane(30.0f, 5.0f);
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));

        _shader = std::make_shared<ShaderProgram>("497VlasovaData/shader.vert", "497VlasovaData/shader.frag");

        ////////////////////////////////////////////
        //Light
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_theta), glm::sin(_phi) * glm::cos(_theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        /////////////////////////////////////////////
        //Textures
        _barkTexture = loadTexture("497VlasovaData/bark2.jpg");
        _grassTexture = loadTexture("497VlasovaData/grass.jpg");
        _leafTexture = loadTexture("497VlasovaData/leaves3.png");


        //////////////////////////////////////////////////
        // Tree bark and branches texture
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Grass texture
        glGenSamplers(1, &_grassSampler);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Leaves texture
        glGenSamplers(1, &_leafSampler);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void draw() override
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));
        
        _shader->setVec3Uniform("light.dir", lightDirCamSpace); 
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        {
            _shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
            glActiveTexture(GL_TEXTURE0);        
            glBindSampler(0, _grassSampler);
            _grassTexture->bind();

            _ground->draw();
        }

        {
            glActiveTexture(GL_TEXTURE0);
            glBindSampler(0, _sampler);
            _barkTexture->bind();
            _shader->setIntUniform("diffuseTex", 0);
            _shader->setVec3UniformArray("positions", _positionsVec3);

            _shader->setMat4Uniform("modelMatrix", _tree_and_leaves.first->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree_and_leaves.first->modelMatrix()))));
            _tree_and_leaves.first->drawInstanced(_positionsVec3.size());
        }

        {
            glActiveTexture(GL_TEXTURE0);  
            glBindSampler(0, _leafSampler);
            _leafTexture->bind();
            _shader->setIntUniform("diffuseTex", 0);
            _shader->setVec3UniformArray("positions", _positionsVec3);

            _shader->setMat4Uniform("modelMatrix", _tree_and_leaves.second->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree_and_leaves.second->modelMatrix()))));
            _tree_and_leaves.second->drawInstanced(_positionsVec3.size());
        }


        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
