#pragma once

#include <vector>

#include "common/Mesh.hpp"
#include "lsys.hpp"

std::pair<MeshPtr, MeshPtr> makeTreeAndLeaves(const std::vector<LSystemLine>& lines);

